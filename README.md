# Sudoku

Author: Truman Guan


## Libraries used:
   * jQuery - for easier dom manipulation
   * Parts of html5boilerplate (mainly for normalize.css)

## Structure
    /
    ├── css/
    │   ├── normalize.css
    │   └── main.css
    │
    ├── js/
    │   └── sudoku.js
    │
    └── index.html



## Comments
Trade-offs:
Heavily used css `nth-child` selector for styling the borders.
This can cause problems rendering in older browsers such as IE8 and below

The implementation of verifying the solution assumes that there's only one solution for initial puzzle.
So instead of looping through each row, column, and 3x3 grids, I instead compared the current board to the predetermined solution.

If I had additional time, I would add additional features such as a generator and a difficulty level.
Code is currently relatively short. If more additional features were to be added, I'd have to separate the object into
separate objects.