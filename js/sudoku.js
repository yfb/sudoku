function Sudoku($el, jQuery){
    "use strict";

    var self = {};
    var $ = jQuery;

    // TODO verify that this is a DOM element
    self.el = $el;

    self.boardSolution = []; // 2d Array
    self.boardCurrentState = [];  // 2d Array


    // TODO: make this generate a new solution instead of using hardcoded one
    function getNewBoard() {
        return [
            [5, 3, 4, 6, 7, 8, 9, 1, 2],
            [6, 7, 2, 1, 9, 5, 3, 4, 8],
            [1, 9, 8, 3, 4, 2, 5, 6, 7],
            [8, 5, 9, 7, 6, 1, 4, 2, 3],
            [4, 2, 6, 8, 5, 3, 7, 9, 1],
            [7, 1, 3, 9, 2, 4, 8, 5, 6],
            [9, 6, 1, 5, 3, 7, 2, 8, 4],
            [2, 8, 7, 4, 1, 9, 6, 3, 5],
            [3, 4, 5, 2, 8, 6, 1, 7, 9]
        ];
    };

    // TODO make this random
    // given a solution, return a puzzle from it
    // 0 represents no value
    function getPuzzle(boardValues) {
        return [
            [5, 3, 4, 0, 7, 8, 9, 0, 2],
            [6, 7, 0, 1, 0, 5, 3, 0, 8],
            [1, 9, 8, 0, 4, 2, 5, 6, 7],
            [0, 5, 0, 7, 6, 1, 0, 2, 3],
            [4, 2, 6, 8, 0, 3, 7, 9, 1],
            [7, 0, 3, 0, 2, 0, 8, 0, 6],
            [9, 6, 1, 5, 3, 7, 2, 8, 4],
            [2, 0, 0, 4, 1, 9, 0, 0, 5],
            [3, 4, 5, 2, 0, 6, 1, 7, 0]
        ];
    }

    function renderSkeleton() {
        var new_html = "";
        new_html += '<div id="gameboard"></div>';

        new_html += '' +
            '<div id="menu_container">' +
            '   <div id="menu"></div>' +
            '   <div id="message"></div>' +
            '   <div id="hint_section"></div>' +
            '</div>';

        $(self.el).html(new_html);
    }

    function renderBoard(boardValues) {
        var new_html = "";
        for (var i = 0; i < 9; i++) {
            new_html += '<div class="row">';
            for (var j = 0; j < 9; j++) {
                var value = boardValues[i][j];
                new_html += '<div class="slot" id="slot_' + i + '_' + j + '">';
                if (value > 0) {
                    new_html += '<input unselectable="on" class="filled slot_input" maxlength="1" value="' + value + '" type="text" readonly="readonly" />';
                } else {
                    new_html += '<input min="1" max="9" class="fillable slot_input"  maxlength="1" type="text" />'
                }
                new_html += "</div>";
            }
            new_html += '</div>';
        }


        $(self.el).find('#gameboard').html(new_html);

        resizeToSquare();
        resizeInputFont();
    }


    function renderHintButton() {
        var new_html = "";

        new_html = '<button id="help_me_out" class="btn">Help me out</button>';

        $(self.el).find('#menu').append(new_html);
    }

    function renderCheckSolutionButton() {
        var new_html = '';
        new_html += '<button id="verify_answer" class="btn">Verify Answer</button>';
        $(self.el).find('#menu').append(new_html);
    }


    function attachSlotHandler() {
        $(self.el).on('keydown', '.fillable', preventInvalidCharListener)
        $(self.el).on('keyup', '.fillable', slotChangeListener)
        $(self.el).on('focus, click, keydown', '.fillable', slotFocusListener)
    }

    function attachButtonHandler() {
        $(self.el).on('click', '#verify_answer', buttonHandler);



        $(self.el).on('click', '#help_me_out', function() {
            var unfilledSlot = getUnfilledSlot();

            if (unfilledSlot != null) {
                var solution = self.boardSolution[unfilledSlot.y][unfilledSlot.x];
                $(self.el).find('#slot_' + unfilledSlot.y + '_' + unfilledSlot.x).find('.slot_input').val(solution);
                updateBoardCurrentState($(self.el).find('#slot_' + unfilledSlot.y + '_' + unfilledSlot.x), solution);
            }
        });

    }

    function buttonHandler(e) {
        var isWin = isBoardMatchSolution();
        var message = "";

        if (isWin) {
            message = '<div class="alert alert_success" style="display:none;">Your solution is correct!</div>';
        } else {
            message = '<div class="alert alert_danger" style="display:none;">Your solution is incorrect.</div>';
        }
        $(self.el).find('#message').html(message);
        $(self.el).find('#message').find('.alert').fadeIn('fast');

    }


    function slotFocusListener(e) {
        this.select();
    }

    function preventInvalidCharListener(e) {
        var charCode = (e.which) ? e.which : event.keyCode;
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            e.preventDefault();
        }

    }



    function slotChangeListener(e) {
        $(self.el).find('#message').html('');
        var slot_changed = $(this).parent('.slot');
        var new_value = parseInt($(this).val(), 10);
        updateBoardCurrentState(slot_changed, new_value);
    }

    function getUnfilledSlot() {
        var unfilled = [];
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                if (self.boardCurrentState[i][j] === 0) {
                    unfilled.push({
                        x: j,
                        y: i
                    });
                }
            }
        }

        if (unfilled.length > 0) {
            var rand = Math.floor(Math.random() * unfilled.length);
            return unfilled[rand];
        } else {
            return null;
        }
    }



    function getCoordsFromSlot(slot_changed) {
        var id = $(slot_changed).attr('id');
        var regex = /slot_(\d)_(\d)/i;
        var coords = id.match(regex);
        return {
            x: coords[1],
            y: coords[2]
        };

    }

    function updateBoardCurrentState(el, value) {
        var coords = getCoordsFromSlot(el);

        self.boardCurrentState[coords.x][coords.y] = value;
    }


    function isBoardMatchSolution() {
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                if (self.boardCurrentState[i][j] != self.boardSolution[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }


    // resize each slot so they are a square
    function resizeToSquare() {

        var size = getOptimalSquareSize();

        var margin_of_error = 4;

        $(self.el).find('.slot').innerWidth(size - margin_of_error);
        $(self.el).find('.slot').innerHeight(size - margin_of_error);
    }

    // resize the font based on the height
    function resizeInputFont() {
        var height = $(self.el).find('.slot').first().innerHeight();
        $(self.el).find('.slot_input').css('font-size', height * (2/3));
    }

    function getOptimalSquareSize() {
        var total_width = $(self.el).find('#gameboard').innerWidth();


        var num_of_9_by_9_borders = 2;
        var size_of_9_by_9_borders = 2;

        var num_of_3_by_3_borders = 2;
        var size_of_3_by_3_borders = 2;

        var num_of_1_by_1_borders = 6;
        var size_of_1_by_1_borders = 1;

        var size = (total_width
            - num_of_1_by_1_borders*size_of_1_by_1_borders
            - num_of_3_by_3_borders*size_of_3_by_3_borders
            - num_of_9_by_9_borders*size_of_9_by_9_borders) / 9;

        return Math.floor(size);
    }

    self.start = function(){

        renderSkeleton();

        self.boardSolution = getNewBoard();

        self.boardCurrentState = getPuzzle(self.boardSolution);
        renderBoard(self.boardCurrentState);


        attachSlotHandler();

        renderCheckSolutionButton();
        renderHintButton();

        attachButtonHandler();
    };






    return self;
}